import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

const logger = new Logger('Main');

const productMicroServiceOptions = {
  name: 'PRODUCT SERVICE',
  transport: Transport.REDIS,
  options: {
    url: 'redis://0.0.0.1:6379',
  },
};

async function bootstrap() {
  // const app = await NestFactory.create(AppModule);
  // await app.listen(3000);

  const app = await NestFactory.createMicroservice(
    AppModule,
    productMicroServiceOptions
  );

  app.listen();
}
bootstrap();
