import { Controller, Get, Post, Body, Patch, Param, Delete, Logger } from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { MessagePattern } from '@nestjs/microservices';

@Controller('products')
export class ProductsController {
  logger = new Logger('Products');
  constructor(private readonly productsService: ProductsService) {}

  @MessagePattern({ cmd: 'addProduct' })
  async create(@Body() createProductDto: CreateProductDto) {
    return await this.productsService.create(createProductDto);
  }

  @MessagePattern({ cmd: 'getProduct' })
  async findAll() {
    this.logger.log('finadAll');
    return await this.productsService.findAll();
  }

  @MessagePattern({ cmd: 'getProductByID' })
  async findOne(@Param('productID') productID: string) {
    this.logger.log('findOne');
    return await this.productsService.findOne(productID);
  }

  @MessagePattern({ cmd: 'updateProductByID' })
  async update(data) {
    this.logger.log('updateupdate');
    return await this.productsService.update(data.productID, data.updateProductDTO);
  }

  @MessagePattern({ cmd: 'viewProductImages' })
  async viewProductImages(@Param('productID') productID: string) {
    this.logger.log('viewProductImages');
    return await this.productsService.getProductImages(productID);
  }

  @MessagePattern({ cmd: 'viewImage' })
  async viewImage(@Param('productID') productID: string) {
    this.logger.log('viewImage');
    return await this.productsService.getImage(productID);
  }

  @MessagePattern({ cmd: 'uploadImage' })
  async uploadImage(@Param('productID') productID: string) {
    this.logger.log('uploadImage');
    return await this.productsService.uploadImage(productID);
  }

}
