import { HttpException, Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { PRODUCTS } from './products.mocks';

@Injectable()
export class ProductsService {
  products = PRODUCTS;

  create(createProductDto: CreateProductDto): Promise<any> {
    return new Promise(resolve => {
      this.products.push(createProductDto);
      resolve(this.products)
    });
  }

  findAll(): Promise<any> {
    return new Promise(resolve => resolve(this.products));
  }

  findOne(id: string) {
    return new Promise(resolve => {
      const product = this.products.find($product => $product.id === id);
      if(!product) {
        throw new HttpException('This Product does not exist!!!...', 404);
      }
      resolve(product);
    });
  }

  update(id: string, updateProductDto: UpdateProductDto): Promise<any> {
    return new Promise(resolve => {
      const index = this.products.findIndex($product => $product.id === id);
      if(index == -1) {
        throw new HttpException('This Product doesnot exist...', 404);
      }
      this.products.push(updateProductDto);
      resolve(this.products);
    });
  }

  getProductImages(id: string) {
    return 'sadad';
  }

  getImage(id: string) {
    return 'sadada';
  }

  uploadImage(id: string) {
    return 'dsas';
  }
}
