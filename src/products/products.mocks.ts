// ./src/mocks/books.mock.ts
export const PRODUCTS = [
    {
      id: 'a',
      name: 'First',
      groupId: 1,
      userId: 100,
    },
    {
      id: 'b',
      name: 'Second',
      groupId: 2,
      userId: 200,
    },
    {
      id: 'c',
      name: 'Third',
      groupId: 3,
      userId: 300,
    },
    {
      id: 'd',
      name: 'Fourth',
      groupId: 4,
      userId: 400,
    },
    {
      id: 'e',
      name: 'Fifth',
      groupId: 5,
      userId: 500,
    },
    {
      id: 'f',
      name: 'Sixth',
      groupId: 6,
      userId: 600,
    },
  ];
  