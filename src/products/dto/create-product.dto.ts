export class CreateProductDto {
    readonly id: string;
    readonly name: string;
    readonly groupId: number;
    readonly userId: number;
}
